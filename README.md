Simple algorithm that compares two html documents, and returns copy of the latter one with added tags on changed parts. Like so:

```
From:
<p>Line</p>

To:
<p>New Line</p>

Result:
<p><Change data-ctype="added" class="added>New</change> Line</p>
```

## Setup local development environment:

- npm ci
- npm run prepare

## Run unit tests on the project:

- npm ci
- npm run test:unit

## Run codeclimate locally:

- npm ci
- ./codeclimate.sh

Note: First run takes quite a long time to complete
