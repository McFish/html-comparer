# 17.6.2024

- Updated packages
- Fixed some codeclimate issues

# 10.4.2024

- Updated packages
- Code cleanup
- Added + fixed failing unit test

# 8.3.2024

- Ported from cheerio to node-html-parser, since development of cheerio stalled
- Updated typescript from 4 to 5
- Updated node version from 18 to 20
