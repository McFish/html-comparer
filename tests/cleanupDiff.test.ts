import { diffWords } from 'diff';
import { cleanupDiffList } from '../src/utils/preProcessors';

describe('Test diff cleanup', () => {
  test('Make sure list is cleaned properly', () => {
    const diff1 = diffWords('Is a test', 'Is not a test', { ignoreWhitespace: false });
    const res1 = cleanupDiffList(diff1);
    expect(res1).toMatchObject([{ value: 'Is ' }, { value: 'not ', added: true }, { value: 'a test' }]);

    const diff2 = diffWords('Is not a test', 'Is a test', { ignoreWhitespace: false });
    const res2 = cleanupDiffList(diff2);
    expect(res2).toMatchObject([{ value: 'Is ' }, { value: 'not ', removed: true }, { value: 'a test' }]);

    const diff3 = diffWords('test line', 'new content', { ignoreWhitespace: false });
    const res3 = cleanupDiffList(diff3);
    expect(res3).toMatchObject([
      { value: 'test', removed: true },
      { value: 'new', added: true },
      { value: ' ' },
      { value: 'line', removed: true },
      { value: 'content', added: true }
    ]);

    const diff4 = diffWords('Is text test', 'Is small new test', { ignoreWhitespace: false });
    const res4 = cleanupDiffList(diff4);
    expect(res4).toMatchObject([
      { value: 'Is ' },
      { value: 'text', removed: true },
      { value: 'small', added: true },
      { value: ' ' },
      { value: 'new ', added: true },
      { value: 'test' }
    ]);
  });
});
