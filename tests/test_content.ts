export const document1 = `
<div>
<p>This is line 1</p>
<p>This is line 2</p>
<p>This is line 3</p>
<p>This is line 4</p>
</div>
`;

export const document2 = `
<div>
  <p>This is line
x
</p>
 <p>
This is not line 2</p>
 <p></p>
 <!-- Comment
 to
 be
 remove -->
</div>
`;
