import { compareDocuments } from '../src/index';

describe('Text comparison', () => {
  test('Only text changes', () => {
    // Add new text to document
    let result = compareDocuments('<div>Is a test</div>', '<div>Is not a test</div>', {});
    expect(result).toBe('<div>Is <change data-ctype="added" class="added">not </change>a test</div>');

    // Replace one word with two (fixme: change should be in one change tag)
    result = compareDocuments('<div>Is text test</div>', '<div>Is small new test</div>', {});
    expect(result).toBe(
      '<div>Is <change data-ctype="removed" class="removed">text</change>' +
        '<change data-ctype="added" class="added">small</change> ' +
        '<change data-ctype="added" class="added">new </change>' +
        'test</div>'
    );

    // Replace both words
    result = compareDocuments('<div>test text</div>', '<div>new content</div>', {});
    expect(result).toBe(
      '<div><change data-ctype="removed" class="removed">test</change>' +
        '<change data-ctype="added" class="added">new</change> ' +
        '<change data-ctype="removed" class="removed">text</change>' +
        '<change data-ctype="added" class="added">content</change>' +
        '</div>'
    );

    // Try adding html entities to document
    result = compareDocuments(
      '<div>Entities: &Aring; &auml;</div>',
      '<div>Entities: &Aring; &auml;&ouml;&aring;</div>',
      {}
    );
    expect(result).toBe(
      '<div>Entities: &Aring; <change data-ctype="removed" class="removed">&auml;</change>' +
        '<change data-ctype="added" class="added">&auml;&ouml;&aring;</change></div>'
    );

    // Remove text from document
    result = compareDocuments('<div>Is not a test</div>', '<div>Is a test</div>', {});
    expect(result).toBe('<div>Is <change data-ctype="removed" class="removed">not </change>a test</div>');

    // Change text inside document
    result = compareDocuments('<div>Is a test</div>', '<div>Is a line</div>', {});
    expect(result).toBe(
      '<div>Is a ' +
        '<change data-ctype="removed" class="removed">test</change>' +
        '<change data-ctype="added" class="added">line</change>' +
        '</div>'
    );

    // Both add and remove texts from document
    result = compareDocuments(
      '<div><p>This is a test</p><p>Second test line</p></div>',
      '<div><p>This is not a test</p><p>Second line</p></div>',
      {}
    );
    expect(result).toBe(
      '<div><p>This is <change data-ctype="added" class="added">not </change>a test</p>' +
        '<p>Second <change data-ctype="removed" class="removed">test </change>line</p></div>'
    );

    // Change only the latter child node
    result = compareDocuments(
      '<div><p>Is a test</p><p>Is not a test</p></div>',
      '<div><p>Is a test</p><p>Is a test</p></div>',
      {}
    );
    expect(result).toBe(
      '<div><p>Is a test</p><p>Is <change data-ctype="removed" class="removed">not </change>a test</p></div>'
    );
  });

  test('Split text in 2', () => {
    // Regression: previously this caused crash
    const result = compareDocuments('<div>test one.</div>', '<div>test <span>two</span>.</div>', {});

    expect(result).toBe(
      '<div>test <change data-ctype="removed" class="removed">one.</change>' +
        '<change data-ctype="added" class="added"><span>two</span></change>' +
        '<change data-ctype="added" class="added">.</change></div>'
    );
  });

  test('Text changes with custom tag', () => {
    // Add text to document, and use custom tag
    let result = compareDocuments('<div>Is a test</div>', '<div>Is not a test</div>', {
      changeTag: 'span'
    });
    expect(result).toBe('<div>Is <span data-ctype="added" class="added">not </span>a test</div>');

    // Both add and remove texts from document, but with custom css tags
    result = compareDocuments(
      '<div><p>This is a test</p><p>Second test line</p></div>',
      '<div><p>This is not a test</p><p>Second line</p></div>',
      { addedClass: 'new', removedClass: 'old' }
    );
    expect(result).toBe(
      '<div><p>This is <change data-ctype="added" class="new">not </change>a test</p>' +
        '<p>Second <change data-ctype="removed" class="old">test </change>line</p></div>'
    );

    // Same as previous with even more custom tags
    result = compareDocuments(
      '<div><p>This is a test</p><p>Second test line</p></div>',
      '<div><p>This is not a test</p><p>Second line</p></div>',
      {
        addedClass: 'new',
        removedClass: 'old',
        changeTag: 'span',
        extraData: [{ name: 'ctime', value: '123456' }]
      }
    );
    expect(result).toBe(
      '<div><p>This is <span data-ctype="added" data-ctime="123456" class="new">not </span>a test</p>' +
        '<p>Second <span data-ctype="removed" data-ctime="123456" class="old">test </span>line</p></div>'
    );
  });

  test('Text and non meaningful structual changes', () => {
    // Add new text and line breaks/white spaces (which should be both ignored) to document
    let result = compareDocuments('<div><p>Is a test</p></div>', '<div>\n <p>Is not a test</p>\n </div>', {});
    expect(result).toBe('<div>\n <p>Is <change data-ctype="added" class="added">not </change>a test</p>\n </div>');

    // Add new text, line breaks, white spaces and html comment (should be ignored also) to document
    result = compareDocuments(
      '<div><p>Is a test</p></div>',
      '<div>\n<p>Is not a test</p>\n <!-- Test -->  \n</div>',
      {}
    );
    expect(result).toBe(
      '<div>\n' +
        '<p>Is <change data-ctype="added" class="added">not </change>a test</p>\n' +
        ' <!-- Test -->  \n</div>'
    );
  });
});
