import { compareDocuments } from '../src/index';

describe('Structual changes', () => {
  test('Simple additional changes in structure', () => {
    let result = '';

    // Add white space between elements (should not affect result in any way)
    result = compareDocuments('<p>Test1</p><p>Test2</p>', ' <p>Test1</p> <p>Test2</p> ', {});
    expect(result).toBe(' <p>Test1</p> <p>Test2</p> ');

    // Add new <p> tag to document (at the end of the nodelist)
    result = compareDocuments('<div><p>Is a test</p></div>', '<div><p>Is a test</p><p>Newline</p></div>', {});
    expect(result).toBe('<div><p>Is a test</p><change data-ctype="added" class="added"><p>Newline</p></change></div>');

    // Add new <p> tag to document (at the beginning of the nodelist)
    result = compareDocuments('<div><p>Is a test</p></div>', '<div><p>Newline</p><p>Is a test</p></div>', {});
    expect(result).toBe('<div><change data-ctype="added" class="added"><p>Newline</p></change><p>Is a test</p></div>');

    // Add multiple different (img & br) tags to document (at the end of the nodelist)
    // <br> tag is also written in wrong format when using xmlmode (should be <br/>)
    result = compareDocuments('<div><p>Is a test</p></div>', '<div><p>Is a test</p><br><img src="tstsrc"/></div>', {});
    expect(result).toBe(
      '<div>' +
        '<p>Is a test</p>' +
        '<change data-ctype="added" class="added"><br></change>' +
        '<change data-ctype="added" class="added"><img src="tstsrc"></change>' +
        '</div>'
    );

    // Add new <div> tag to document (at the beginning and end of the nodelist)
    result = compareDocuments('<div><p>Is a test</p></div>', '<div><div>Newline</div><p>Is a test</p></div>', {});
    expect(result).toBe(
      '<div><change data-ctype="added" class="added"><div>Newline</div></change><p>Is a test</p></div>'
    );
    result = compareDocuments('<div><p>Is a test</p></div>', '<div><p>Is a test</p><div>Newline</div></div>', {});
    expect(result).toBe(
      '<div><p>Is a test</p><change data-ctype="added" class="added"><div>Newline</div></change></div>'
    );

    // Add new <div>tags to document (at the middle of the nodelist)
    result = compareDocuments(
      '<div><p>Testline 1</p><p>Testline 2</p></div>',
      '<div><p>Testline 1</p><div>Newline</div><p>Testline 2</p></div>',
      {}
    );
    expect(result).toBe(
      '<div><p>Testline 1</p><change data-ctype="added" class="added"><div>Newline</div></change><p>Testline 2</p></div>'
    );

    // Wrap other text element with strong tag
    result = compareDocuments('<p>Test text</p>', '<p><strong>Test</strong> text</p>', {});
    expect(result).toBe(
      '<p><change data-ctype="added" class="added"><strong>Test</strong></change>' +
        '<change data-ctype="removed" class="removed">Test</change> text</p>'
    );

    // Add element after text node (which will have extra space at end)
    result = compareDocuments('<div>Test</div>', '<div>Test <p>line</p></div>', {});
    expect(result).toBe(
      '<div>Test' +
        '<change data-ctype="added" class="added"> </change>' +
        '<change data-ctype="added" class="added"><p>line</p></change></div>'
    );
  });

  test('Modify list', () => {
    let result = '';

    // Add item
    result = compareDocuments('<p><ul><li>1</li></ul></p>', '<p><ul><li>1</li><li>2</li></ul></p>', {});
    expect(result).toBe('<p><ul><li>1</li><change data-ctype="added" class="added"><li>2</li></change></ul></p>');

    // Remove item
    result = compareDocuments('<p><ul><li>1</li><li>2</li></ul></p>', '<p><ul><li>1</li></ul></p>', {});
    expect(result).toBe('<p><ul><li>1</li><change data-ctype="removed" class="removed"><li>2</li></change></ul></p>');
  });

  test('Structure change with custom tags', () => {
    // Add node
    const result1 = compareDocuments('<div><p>Is a test</p></div>', '<div><p>Newline</p><p>Is a test</p></div>', {
      addedClass: 'new',
      changeTag: 'span'
    });
    expect(result1).toBe('<div><span data-ctype="added" class="new"><p>Newline</p></span><p>Is a test</p></div>');

    // Remove node
    const result2 = compareDocuments('<div><div>Newline</div><p>Is a test</p></div>', '<div><p>Is a test</p></div>', {
      removedClass: 'old',
      changeTag: 'span'
    });
    expect(result2).toBe('<div><span data-ctype="removed" class="old"><div>Newline</div></span><p>Is a test</p></div>');
  });

  test('Simple remove changes in structure', () => {
    let result = '';

    // Remove node from end of nodelist
    result = compareDocuments('<div>test<p>Line</p></div>', '<div>test</div>', {});
    expect(result).toBe('<div>test' + '<change data-ctype="removed" class="removed"><p>Line</p></change>' + '</div>');

    // Removed node from between nodelist and also do small changes to text
    const doc1 =
      '<p>1) First line of the document</p>' +
      '<p>2) This will be removed</p>' +
      '<p>3) This one will stay here</p>' +
      '<p>4) And this is the last one</p>';

    const doc2 =
      '<p>1) First line of the document</p>' +
      '<p>2) This one will stay here</p>' +
      '<p>3) And this is the last one</p>';

    const xpected =
      '<p>1) First line of the document</p>' +
      '<change data-ctype="removed" class="removed"><p>2) This will be removed</p></change>' +
      '<p><change data-ctype="removed" class="removed">3</change><change data-ctype="added" class="added">2</change>) This one will stay here</p>' +
      '<p><change data-ctype="removed" class="removed">4</change><change data-ctype="added" class="added">3</change>) And this is the last one</p>';

    result = compareDocuments(doc1, doc2, {});
    expect(result).toBe(xpected);

    result = compareDocuments(`<div>\n${doc1}\n</div>`, `<div>\n${doc2}\n</div>`, {});
    expect(result).toBe(`<div>\n${xpected}\n</div>`);

    // Remove all content
    result = compareDocuments('<p>Line</p>', '', {});
    expect(result).toBe('<change data-ctype="removed" class="removed"><p>Line</p></change>');
  });

  test('Move content one level deeper', () => {
    const result = compareDocuments('<p>Test content</p>', '<p><b>Test content</b></p>', {});
    expect(result).toBe(
      '<p><change data-ctype="removed" class="removed">Test content</change>' +
        '<change data-ctype="added" class="added"><b>Test content</b></change></p>'
    );
  });
});
