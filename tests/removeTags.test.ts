import { removeTags } from '../src/index';

describe('Tests for removing tags', () => {
  test('Basic cleanup', () => {
    // Remove change tags
    let result = removeTags(
      '<div>Is a ' +
        '<change data-ctype="removed" class="removed">test</change>' +
        '<change data-ctype="added" class="added">line</change>' +
        '</div>',
      {}
    );
    expect(result).toBe('<div>Is a line</div>');

    // This should not leave anything behind
    result = removeTags(
      '<div>Is a ' +
        '<change data-ctype="removed" class="removed">test</change>' +
        '<change data-ctype="added" class="added"></change>' +
        '</div>',
      {}
    );
    expect(result).toBe('<div>Is a </div>');

    // Do the same with nested tags
    result = removeTags(
      '<div>Is a ' +
        '<change data-ctype="removed" class="removed"><change data-ctype="removed" class="removed">test</change></change>' +
        '<change data-ctype="added" class="added"><change data-ctype="added" class="added">line</change></change>' +
        '</div>',
      {}
    );
    expect(result).toBe('<div>Is a line</div>');
  });
});
