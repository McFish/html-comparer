import { parse } from 'node-html-parser';
import { DiffOptions } from './types';
import { fixDocumentTags } from './utils/domUtils';
import { compareNodes } from './utils/nodeComparators';
import { addIdsToDocument } from './utils/preProcessors';

const DEFAULT_OPTIONS: DiffOptions = {
  // Extra DATA parameters added to change tag
  extraData: [],

  // CSS-Class to mark added content with
  addedClass: 'added',

  // CSS-Class to mark removed content with
  removedClass: 'removed',

  // Tag to mark changes with
  changeTag: 'change',

  // Should we strip empty text nodes before comparison
  stripEmptyTextNodes: true,

  // Should we strip comments before comparing documents
  stripComments: true
};

/**
 * Remove change tags from given html
 *
 * @param source Source HTML from which to remove change tags
 * @param options Options to override
 * @returns Modified HTML
 */
export const removeTags = (source: string, options: Partial<DiffOptions>): string => {
  const opt = { ...DEFAULT_OPTIONS, ...options };

  const fixedDocument = fixDocumentTags(source);
  const $doc = parse(fixedDocument);

  // Remove all 'removed' tags with content
  $doc.querySelectorAll(`${opt.changeTag}[data-ctype="removed"]`).forEach((tag) => tag.remove());

  // Unwrap all 'added' tags in multiple passes (to remove possible nested tags)
  for (let l1 = 0; l1 < 2; l1++) {
    const tags = $doc.querySelectorAll(`${opt.changeTag}[data-ctype="added"]`);
    if (tags.length === 0) {
      break;
    }
    tags.forEach((tag) => {
      tag.insertAdjacentHTML('afterend', tag.innerHTML);
      tag.remove();
    });
  }

  // Return modified html
  return $doc.toString();
};

/**
 * Compare two html documents and return html document with differences marked with tag
 *
 * @param before Source HTML
 * @param after Destination HTML
 * @param options Options to override
 * @returns Destination document, with changes marked to html structure
 */
export const compareDocuments = (before: string, after: string, options: Partial<DiffOptions>): string => {
  const merged_options = { ...DEFAULT_OPTIONS, ...options };

  // Fix "invalid" tags (<br> -> <br/>)
  before = fixDocumentTags(before);
  after = fixDocumentTags(after);

  // Parse before & after documents and create a copy of after, to which tags will be added
  const $beforeDoc = parse(before, { comment: true });
  const $afterDoc = parse(after, { comment: true });
  const $resDoc = parse(after, { comment: true });

  // Add IDs to nodes
  addIdsToDocument($afterDoc, 1);
  addIdsToDocument($resDoc, 1);

  compareNodes($resDoc, $beforeDoc, $afterDoc, merged_options);

  // Finally return the document with changes highlighted
  return $resDoc.toString();
};
