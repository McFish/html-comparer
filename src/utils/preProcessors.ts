import { Change } from 'diff';
import { HTMLElement } from 'node-html-parser';
import { ExtNode } from '../types';

let idCounter: number;

// These elements can be safely encoded/decoded without breaking document
const EntityMap = [
  ['&Auml;', 'Ä'],
  ['&auml;', 'ä'],
  ['&Ouml;', 'Ö'],
  ['&ouml;', 'ö'],
  ['&Aring;', 'Å'],
  ['&aring;', 'å'],
  ['&gt;', '>'],
  ['&lt;', '<']
];

/**
 * Encode or decode HTML entities in document
 *
 * @param source - Source HTML
 * @param decode - decode (true) or encode (false) ?
 */
export const codeEntities = (source: string, decode: boolean): string => {
  for (const en of EntityMap) {
    source = decode ? source.replaceAll(en[0], en[1]) : source.replaceAll(en[1], en[0]);
  }
  return source;
};

/**
 * Create more readable difference list
 *
 * @param diffs
 */
export const cleanupDiffList = (diffs: Change[]): Change[] => {
  const newDiffs: Change[] = [];

  for (let idx = 0; idx < diffs.length; idx++) {
    const [next1, next2] = diffs.slice(idx + 1, idx + 3);

    // If there is less than 3 elements left, just add them all
    if (idx > diffs.length - 3) {
      newDiffs.push(diffs[idx]);
      continue;
    }

    // Has this and next token changed place? If yes, insert them as is
    if (diffs[idx].removed && next1.added && next2.value === ' ') {
      newDiffs.push(diffs[idx], next1, next2);
      idx += 2;
      continue;
    }

    // Add this item as is, if next one is not space
    if (next1.value !== ' ') {
      newDiffs.push(diffs[idx]);
    } else {
      // Does the last item contain trailing whitespaces?
      const res = next2.value.match(/(.*?)(\s+)$/);

      // Add combined string, and trailing whitespace if found
      newDiffs.push({ count: 1, added: true, removed: false, value: `${diffs[idx].value} ${res?.[1] ?? ''}` });

      if (res && res.length === 3) {
        newDiffs.push({ count: 1, added: false, removed: false, value: res[2] });
      }

      idx += 2;
    }
  }

  return newDiffs;
};

/**
 * Add ID value to given document
 *
 * @param $rootNode
 * @param initValue
 */
export const addIdsToDocument = ($rootNode: HTMLElement, initValue: number) => {
  idCounter = initValue;
  $rootNode.childNodes.forEach((node) => addIdsToNodes(node as ExtNode));
};

const addIdsToNodes = ($node: ExtNode) => {
  if (!$node._id) {
    $node._id = idCounter++;
  }

  for (const el of $node.childNodes) {
    addIdsToNodes(el as ExtNode);
  }
};
