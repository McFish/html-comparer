import { createHash } from 'crypto';
import { DiffOptions, ExtNode, NODE_TYPES } from '../types';

/**
 * Create the change HTML tag
 *
 * @param content
 * @param type
 * @param className
 * @param options
 */
export const createChangeTag = (content: string, type: string, className: string, options: DiffOptions): string => {
  const dataFields: string[] = options.extraData.map((d) => `data-${d.name}="${d.value}" `);
  const extraTags = dataFields.length === 0 ? ' ' : ' ' + dataFields.join(' ');
  return (
    `<${options.changeTag} data-ctype="${type}"` + extraTags + `class="${className}">${content}</${options.changeTag}>`
  );
};

/**
 * Create SHA256 hash of the node and it's subnodes
 *
 * @param node
 * @param level
 * @returns
 */
export const calculateNodeHash = (node: ExtNode, level: number): string => {
  let attributes = '';
  for (const key of Object.keys(node.attributes ?? {})) {
    const name = key;
    const value = node.attributes[key].replaceAll(/\s/g, '');
    attributes += `${name}=${value}`;
  }

  let hash = `${node.nodeType}:${node.nodeType === NODE_TYPES.TEXT_NODE ? node.text : node.rawTagName}${attributes ? ':' + attributes : ''}`;

  for (const child of node.childNodes) {
    hash += ';' + calculateNodeHash(child as ExtNode, level + 1);
  }

  return level === 0 ? createHash('sha256').update(hash).digest('hex') : hash;
};

/**
 * Fix document to xml compliant format
 *
 * @param source
 * @returns
 */
export const fixDocumentTags = (source: string): string => {
  const retval = source.replace(/<br>/gi, '<br/>');
  return retval;
};

/**
 * Find node from document with given id
 *
 * @param $rootNode
 * @param id
 * @returns
 */
export const findNodeWithId = ($rootNode: ExtNode, id: number): ExtNode | null => {
  let retval: ExtNode | null = null;

  for (const node of $rootNode.childNodes) {
    retval = findSubNodeWithId(node as ExtNode, id);

    // Break loop when something is found (retval !== null)
    if (retval !== null) break;
  }

  return retval;
};

const findSubNodeWithId = (node: ExtNode, id: number): ExtNode | null => {
  if (node._id === id) {
    return node;
  }

  let retval: ExtNode | null = null;
  for (const el of node.childNodes) {
    retval = findSubNodeWithId(el as ExtNode, id);
    if (retval !== null) {
      return retval;
    }
  }

  return retval;
};

/**
 * Dump documents DOM structure to console
 *
 * @param $rootNode
 */
export const dumpDomToConsole = ($rootNode: ExtNode) => {
  $rootNode.childNodes.forEach(($node) => {
    dumpNodeToConsole($node as ExtNode, 1);
  });
};

const dumpNodeToConsole = ($node: ExtNode, level: number) => {
  const content = $node.nodeType === NODE_TYPES.TEXT_NODE ? `Text: ${$node.text}` : `tag: ${$node.rawTagName}`;
  console.log(`${' '.repeat(level - 1)}Node type: ${$node.nodeType}, ${content}, id: ${$node._id}`);
  for (const el of $node.childNodes) {
    dumpNodeToConsole(el as ExtNode, level + 1);
  }
};
