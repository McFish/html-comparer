import { Change, diffWords } from 'diff';
import { parse, HTMLElement, Node } from 'node-html-parser';
import { DiffOptions, ExtNode, NODE_TYPES } from '../types';
import { calculateNodeHash, createChangeTag, findNodeWithId } from './domUtils';
import { calcLevenshtein } from './levenshtein';
import { cleanupDiffList, codeEntities } from './preProcessors';

/**
 * Compare two content nodes (1 text child) and return true if they match (or are close enough)
 *
 * @param n1
 * @param n2
 */
const isSameContent = (n1: Node, n2: Node) => {
  const c1 = n1.childNodes[0].rawText;
  const c2 = n2.childNodes[0].rawText;

  // Levenshtein = number of changes needed to convert c1 to c2
  return c1 === c2 || calcLevenshtein(c1, c2) < 5;
};

const isTextContainer = (n1: Node) => {
  if (n1.nodeType !== NODE_TYPES.ELEMENT_NODE || n1.childNodes.length !== 1) return false;

  return n1.childNodes[0].nodeType === NODE_TYPES.TEXT_NODE;
};

const isSameNode = (n1: Node, n2: Node) => {
  if (n1.nodeType !== n2.nodeType) return false;

  if (n1.nodeType === NODE_TYPES.ELEMENT_NODE) {
    if (n1.rawTagName !== n2.rawTagName) return false;
  }

  return true;
};

const isEmptyTextNode = (n: Node) => {
  return n.nodeType === NODE_TYPES.TEXT_NODE && n.rawText.replace(/(\r\n|\n|\r|\s)/gm, '') === '';
};

const markAsRemoved = ($result: HTMLElement, childs: Node[], l1: number, nodeId: number, options: DiffOptions) => {
  const newChild = parse(createChangeTag('', 'removed', options.removedClass, options));
  if (!newChild.firstChild) return;

  const removedNode = childs[l1] as ExtNode;
  removedNode._processed = true;
  const outerHTML = removedNode.nodeType === NODE_TYPES.ELEMENT_NODE ? removedNode.outerHTML : removedNode.rawText;

  const chNode = newChild.firstChild as HTMLElement;
  chNode.innerHTML = outerHTML;

  const tgtNode = nodeId > 0 ? findNodeWithId($result, nodeId) : $result;
  if (tgtNode) {
    chNode.parentNode = tgtNode;
    if (l1 < tgtNode.childNodes.length) {
      tgtNode.childNodes.splice(l1, 0, chNode);
    } else {
      tgtNode.appendChild(chNode);
    }
  }
};

const markAsAdded = ($result: HTMLElement, nodeId: number, options: DiffOptions) => {
  const node = nodeId > 0 ? findNodeWithId($result, nodeId) : $result;

  // Ignore comment and text nodes with only whitespace
  if (node === null || node.nodeType === NODE_TYPES.COMMENT_NODE || isEmptyTextNode(node)) {
    return;
  }

  // Text node doesn't currenly have replacewith method, so we need to use temp node
  if (node.nodeType === NODE_TYPES.TEXT_NODE) {
    const tmpNode = new HTMLElement('p', {}, '', node.parentNode);
    node.parentNode.exchangeChild(node, tmpNode);

    const changeTag = createChangeTag(node.rawText, 'added', options.addedClass, options);
    tmpNode.replaceWith(changeTag);
  } else {
    const changeTag = createChangeTag(node.outerHTML, 'added', options.addedClass, options);
    node.replaceWith(changeTag);
  }
};

const findNextNode = (childs: Node[], idx: number | null) => {
  if (idx === null || idx + 1 >= childs.length) return null;
  for (let l1 = idx + 1; l1 < childs.length; l1++) {
    if (!isEmptyTextNode(childs[l1])) return l1;
  }
  return null;
};

const findRemovedItems = (
  n1c: Node[],
  n2c: Node[],
  $result: HTMLElement,
  $after: HTMLElement,
  options: DiffOptions
) => {
  let l2Idx = 0;
  let l1: number | null = -1;

  while ((l1 = findNextNode(n1c, l1)) !== null) {
    if (l2Idx < n2c.length) {
      let found = false;
      for (let l2 = l2Idx; l2 < n2c.length; l2++) {
        if (!isSameNode(n1c[l1], n2c[l2])) continue;
        if (isTextContainer(n1c[l1]) && isTextContainer(n2c[l2]) && !isSameContent(n1c[l1], n2c[l2])) {
          const nextc1 = findNextNode(n1c, l1);
          const nextc2 = findNextNode(n2c, l2Idx);
          if (nextc1 !== null && nextc2 !== null && isSameNode(n1c[nextc1], n2c[nextc2])) {
            continue;
          }
        }

        // + 1, because we need to advance the search past the found item
        l2Idx = l2 + 1;
        found = true;
        break;
      }

      if (found) continue;
    }

    markAsRemoved($result, n1c, l1, ($after as ExtNode)?._id ?? 0, options);
  }
};

/**
 * Compare two html nodes
 *
 * @param $result
 * @param $before
 * @param $after
 * @param options
 */
export const compareNodes = ($result: HTMLElement, $before: HTMLElement, $after: HTMLElement, options: DiffOptions) => {
  // Compare content of nodes (mostly text nodes at this time)
  compareNodeContents($result, $before, $after, options);

  // Neither document has nodes .. all done
  if ($before.childNodes.length === 0 && $after.childNodes.length === 0) {
    return;
  }

  const n1c = $before.childNodes;
  const n2c = $after.childNodes;

  // Calculate sha256 hash of all subnodes
  const n1cHtml = n1c.map((node) => calculateNodeHash(node as ExtNode, 0));
  const n2cHtml = n2c.map((node) => calculateNodeHash(node as ExtNode, 0));

  // Iterate node1 children (search for removed nodes)
  findRemovedItems(n1c, n2c, $result, $after, options);

  // Iterate node2 children (search for new or changed nodes)
  let l1 = 0;

  for (let l2 = 0; l2 < $after.childNodes.length; l2++) {
    const n1child = $before.childNodes[l1] as ExtNode;
    const n2child = $after.childNodes[l2] as ExtNode;

    if (isEmptyTextNode(n2child)) continue;

    if (l1 < n1c.length) {
      // Node and their subnodes match
      if (n2cHtml[l2] === n1cHtml[l1]) {
        l1++;
        continue;
      }

      // If node is already marked as removed or just an empty text node, skip it
      if (n1child._processed === true || isEmptyTextNode(n1c[l1])) {
        l1++;
        l2--;
        continue;
      }

      // new node has been added before existing node
      if (l2 + 1 < $after.childNodes.length && n2cHtml[l2 + 1] === n1cHtml[l1]) {
        markAsAdded($result, n2child?._id ?? 0, options);
        continue;
      }
    }

    // This is new node, if one of these tests match:
    // (1) Node1 has no more children left
    // (2) Both nodes are element nodes, but tags don't match (div vs p)
    // (3) Or nodes are of completely different
    if (l1 >= n1c.length || !isSameNode(n1child, n2child)) {
      markAsAdded($result, n2child?._id ?? 0, options);
      continue;
    }

    // Compare nodes
    compareNodes($result, n1child, n2child, options);

    // Advance to next node in node1 children
    l1++;
  }
};

export const compareNodeContents = ($result: ExtNode, $before: ExtNode, $after: ExtNode, options: DiffOptions) => {
  switch ($before.nodeType) {
    case NODE_TYPES.TEXT_NODE: {
      const compareRes = compareTextNodes($before, $after, options);

      if (compareRes !== null && $after._id) {
        const tgtNode = findNodeWithId($result, $after._id);
        if (tgtNode !== null) {
          // Use temp element, since text node doesn't have replaceWith method
          const tmpNode = new HTMLElement('p', {}, '', tgtNode.parentNode);
          tgtNode.parentNode.exchangeChild(tgtNode, tmpNode);
          tmpNode.replaceWith(compareRes);
        }
      }
      break;
    }
    case NODE_TYPES.ELEMENT_NODE: {
      compareTagNodes($before, $after);
      break;
    }
    default:
      // console.log(`Unsupported node type: ${$before.nodeType}`);
      break;
  }
};

const compareTextNodes = (node1: ExtNode, node2: ExtNode, options: DiffOptions): string | null => {
  const text1 = node1.text;
  const text2 = node2.text;

  if (text1 === text2) {
    return null;
  }

  const diffList = cleanupDiffList(
    diffWords(codeEntities(text1, true), codeEntities(text2, true), { ignoreWhitespace: false })
  );

  return diffList.reduce((acc: string, part: Change) => {
    if (part.added) {
      return acc + createChangeTag(codeEntities(part.value, false), 'added', options.addedClass, options);
    } else if (part.removed) {
      return acc + createChangeTag(codeEntities(part.value, false), 'removed', options.removedClass, options);
    }
    return acc + codeEntities(part.value, false);
  }, '');
};

const compareTagNodes = (node1: ExtNode, node2: ExtNode): null => {
  // TODO: At the moment we only compare tag names (div, span, etc).. we should compare also attributes
  if (node1.rawTagName === node2.rawTagName) {
    return null;
  }

  // console.log(`Tag nodes do not match: ${node1.rawTagName} != ${node2.rawTagName}`);
  return null;
};
