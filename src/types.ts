import { HTMLElement } from 'node-html-parser';

export const NODE_TYPES = {
  ELEMENT_NODE: 1,
  TEXT_NODE: 3,
  COMMENT_NODE: 8
};

export type DataField = {
  name: string;
  value: string;
};

export interface ExtNode extends HTMLElement {
  _id?: number;
  _processed?: boolean;
}

export type DiffOptions = {
  addedClass: string;
  removedClass: string;
  changeTag: string;
  extraData: DataField[];
  stripEmptyTextNodes: boolean;
  stripComments: boolean;
};
